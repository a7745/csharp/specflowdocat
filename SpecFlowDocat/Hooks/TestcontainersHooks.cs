﻿using BoDi;
using DotNet.Testcontainers.Builders;
using DotNet.Testcontainers.Containers;

namespace SpecFlowDocat.Hooks
{
    [Binding]
    public sealed class TestcontainersHooks
    {
        private readonly IContainer _container;

        public TestcontainersHooks(IObjectContainer objectContainer)
        {
            _container = new ContainerBuilder()
            .WithImage("robinmatz/docat")
            .WithPortBinding(80, true)
            .WithWaitStrategy(Wait.ForUnixContainer().UntilHttpRequestIsSucceeded(r => r.ForPort(80)))
            .Build();
            objectContainer.RegisterInstanceAs<IContainer>(_container);
        }

        [BeforeScenario]
        public async Task StartContainer()
        {
            await _container.StartAsync().ConfigureAwait(false);
        }
    }
}