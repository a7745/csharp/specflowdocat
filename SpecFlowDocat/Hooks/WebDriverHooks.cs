﻿using BoDi;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace SpecFlowDocat.Hooks
{
    [Binding]
    public sealed class WebDriverHooks
    {
        private readonly IObjectContainer _container;
        private IWebDriver? _driver;

        public WebDriverHooks(IObjectContainer container)
        {
            _container = container;
        }

        [BeforeScenario]
        public void RegisterWebDriver()
        {
            _driver = new ChromeDriver();
            _container.RegisterInstanceAs<IWebDriver>(_driver);
        }

        [AfterScenario]
        public void UnregisterWebDriver()
        {
            _driver?.Dispose();
        }
    }
}