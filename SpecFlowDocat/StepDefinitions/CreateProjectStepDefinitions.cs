using DotNet.Testcontainers.Containers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SpecFlowDocat.DocatService;

namespace SpecFlowDocat.StepDefinitions
{
    [Binding]
    public class CreateProjectStepDefinitions
    {
        private readonly IContainer _container;

        private readonly IWebDriver _driver;

        public CreateProjectStepDefinitions(IContainer container, IWebDriver driver)
        {
            _container = container;
            _driver = driver;
        }

        [Given(@"There is no project")]
        public async Task GivenThereIsNoProject()
        {
            var client = new Client($"http://{_container.Hostname}:{_container.GetMappedPublicPort(80)}/", new HttpClient());
            var projects = await client.ApiProjectsGetAsync(true);
            projects.Projects1.Should().BeEmpty();
        }

        [When(@"I navigate to the homepage")]
        public void WhenINavigateToTheHomepage()
        {
            _driver.Navigate().GoToUrl($"http://{_container.Hostname}:{_container.GetMappedPublicPort(80)}");
        }

        [Then(@"I should land on the help page")]
        public void ThenIShouldLandOnTheHelpPage()
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(5))
                .Until(d => d.Title.Equals("Help | docat"));
        }

    }
}
